# Kubernetes | Volume et stockage persistant
_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Ce laboratoire vise à présenter les étapes de mise en place d'un volume persistant sur Kubernetes.

## Objectifs

Dans ce laboratoire, nous allons mettre en place un environnement utilisant Kubernetes pour démontrer l'utilisation de volumes persistants avec MySQL :

1. **Création du Pod MySQL** :
   - Rédigez un manifeste `mysql-volume.yml` pour déployer un pod MySQL nommé `mysql-volume`. Configurez les variables d'environnement pour le nom de la base de données, l'utilisateur, et les mots de passe.
   - Assurez-vous que le répertoire de données MySQL soit persistant en le montant sur `/data-volume`.

2. **Gestion des Volumes** :
   - Écrivez un manifeste `pv.yml` pour créer un volume persistant (PV) de 5 Go, utilisant le dossier local `local/data-pv`.
   - Rédigez un manifeste `pvc.yml` pour un claim de volume persistant (PVC) de 1 Go qui utilise le PV créé précédemment.

3. **Déploiement de MySQL avec PVC** :
   - Déployez MySQL avec le PVC à l'aide d'un nouveau manifeste `mysql-pv.yml`, assurant que MySQL utilise ce volume pour le stockage des données.

4. **Vérifications** :
   - Lancez les ressources et vérifiez que le pod MySQL est bien démarré et utilise le dossier `/data-volume`.
   - Confirmez que les PV et PVC sont correctement créés et prêts à être utilisés.


## 1. Définition des concepts

### 1.1. C'est quoi un Volume Persistant (PV) ?
Un Volume Persistant (PV) dans Kubernetes est une ressource dans le cluster qui permet de stocker des données de manière persistante, indépendamment du cycle de vie des pods qui les utilisent. Les PV sont des volumes de stockage provisionnés par un administrateur ou dynamiquement par Kubernetes à l'aide de classes de stockage. Ils offrent une manière de gérer des volumes de stockage sur une infrastructure sous-jacente, permettant aux utilisateurs de consommer du stockage abstrait sans se préoccuper des détails spécifiques à un environnement ou un fournisseur.


### 1.2. C'est quoi un Persistent Volume Claim (PVC) ? 
Un Persistent Volume Claim (PVC) dans Kubernetes est une requête de stockage. Il permet de demander des ressources de stockage spécifiques (taille et accessibilités), qui sont ensuite liées à des Persistent Volumes (PV) existants dans le cluster selon leur capacité et leurs accès. Un PVC consomme des ressources de PV, agissant comme une sorte de ticket pour l’utilisation de ressources de stockage définies dans un PV. Cela permet une abstraction entre la demande et la provision de stockage, offrant ainsi flexibilité et automatisation.


## 2. **Prérequis** : Liste des exigences matérielles et logicielles.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu 20.04 Focal Fossa LTS, grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws) (recommandé)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)

Nos nodes tournent sous ubuntu 20.04 LTS.



## 2. Création d'un cluster

Consultez le document [Installer kubeadm](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git)


## 3. Déploiement du Pod MySQL
  ### 3.1. Rédaction du manifeste `mysql-volume.yml`.
  ````bash
  sudo nano mysql-volume.yml
  ````

  ````bash
apiVersion: v1
kind: Pod
metadata:
  name: mysql-volume
spec:
  containers:
  - image: mysql
    name: mysql
    volumeMounts:
    - montPath: /var/lib/mysql
      name: mysql-data
    env:
      - name: MYSQL_ROOT_PASSWORD
        value: password
      - name: MYSQL_DATABASE
        value: db-acd 
      - name: MYSQL_USER
        value: acd
      - name: MYSQL_PASSWORD
        value: devops-acd
  volumes:
  - name: mysql-data
    hostPath:
      #Path hote
      path: /data-volume
      type: Directory
  ````

Dans ce manifeste nous définissons un **Pod** Kubernetes nommé `mysql-volume` qui utilise l'image officielle `mysql`. Le Pod est configuré avec un volume nommé `mysql-data` monté sur `/var/lib/mysql`, ce qui indique que les données de MySQL seront stockées dans un répertoire spécifique sur l'hôte. Les variables d'environnement définies dans le manifeste configurent les paramètres de base de données MySQL, y compris le mot de passe root, le nom de la base de données, l'utilisateur, et le mot de passe de l'utilisateur. Le volume `mysql-data` utilise `hostPath` pour lier le répertoire `/data-volume` sur l'hôte physique, assurant la persistance des données.


L'on va crée un repertoire **"/data-volume"** sur nos nodes **worker1** et **worker2**

````bash
sudo mkdir /data-volume
````

````bash
kubectl apply -f mysql-volume.yml
````

>![alt text](img/image.png)

### 3.2. Vérification du contenu de `/data-volume`.

````bash
ls /data-volume/
````

>![alt text](img/image-1.png)

Les données du pod **`mysql-volume`** sont bien stockées sur le volume persistant **/data-volume** situé sur le worker1

même en supprimant notre pod, les données continuerons d'être sauvegardées


## 4. Gestion des volumes
  ### 4.1. Création et configuration du volume persistant (PV) avec `pv.yml`.

  ````bash
  sudo nano pv.yml
  ````

  ````bash
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/data-pv"
````

Ce **Persistent Volume (PV)** est étiqueté comme un stockage local avec une classe de stockage manuelle spécifiée sous `storageClassName: manual`. Le PV est configuré avec une capacité de stockage de 5 Go et un mode d'accès `ReadWriteOnce`, ce qui signifie qu'il peut être monté en lecture-écriture par un seul nœud. Il utilise `hostPath` pour lier le chemin `/data-pv` sur l'hôte, destiné à stocker les données persistantes utilisées par les pods dans le cluster.


````bash
kubectl apply -f pv.yml
````

>![alt text](img/image-2.png)
*Le Persistent Volume est bien crée*

  
  ### 4.2. Création du claim de volume persistant (PVC) avec `pvc.yml`.

  ````bash
  sudo nano pvc.yml 
  ````

  ````yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  ````

Nous définissons ici, une demande de volume persistant **PersistentVolumeClaim (PVC)** dans Kubernetes. Le manifest spécifi l'utilisation de la classe de stockage **manual**, nous respectons ici la configuration qui a été faites pour le **PV** plus haut. Le PVC demande un mode d'accès **ReadWriteOnce**, permettant le montage du volume en lecture et écriture sur un seul nœud. Il demande également une capacité de stockage de **1 Gi**. Il est utilisé pour réserver une partie de l'espace de stockage disponible dans un **Persistent Volume** correspondant qui remplit ces critères.

  ````bash
  kubectl apply -f pvc.yml
  ````
>![alt text](img/image-3.png)
*PVC à été bien crée*

  
  ### 4.3. Explication du lien entre PV, PVC, et leur utilisation.

## 5. Déploiement de MySQL avec stockage persistant
  ### 5.1. Rédaction du manifeste `mysql-pv.yml`.

  ````bash
  sudo nano mysql-pv.yml
  ````
  
  ````yaml
apiVersion: v1
kind: Pod
metadata:
  name: mysql-pv
spec:
  containers:
  - image: mysql
    name: mysql
    volumeMounts:
    - mountPath: /var/lib/mysql
      name: mysql-data
    env:
      - name: MYSQL_ROOT_PASSWORD
        value: password
      - name: MYSQL_DATABASE
        value: db-acd 
      - name: MYSQL_USER
        value: acd
      - name: MYSQL_PASSWORD
        value: devops-acd
  volumes:
  - name: mysql-data
    persistentVolumeClaim:
      claimName: pvc
  ````

Ce manifeste permet de ployer un Pod dans Kubernetes nommé `mysql-pv` qui exécute un conteneur basé sur l'image officielle **`mysql`**. Le Pod est configuré pour utiliser le **PersistentVolumeClaim**  **`pvc`** crée précédement, pour le stockage des données. Le volume est monté sur le chemin **`/var/lib/mysql`** dans le conteneur, ce qui est le répertoire standard où MySQL stocke ses données. Cette configuration assure que les données MySQL sont stockées de manière persistante et restent disponibles même si le Pod est redéployé sur un autre nœud du cluster.


  ### 5.2. Déploiement de MySQL utilisant le PVC.

````bash
kubectl apply -f mysql-pv.yml
````

### 5.3. Vérification et tests

````bash
kubectl describe pod mysql-pv
````

>![alt text](img/image-4.png)

Comme on peut le voir sur la capture, le nouveau pod à été deployé sur le **worker2**, on peut donc se connecter sur ce worker afin de vérifier le repertoire **/data-pv/**

````bash
ls /data-volume
````

>![alt text](img/image-5.png)

Sur le worker2, les données du pod **mysql-pv** sont bien stockées et sauvegardées dans le repertoire **/data-volume**



## Documentation 

### [Installer kubeadm sur ubuntu](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

### [Creating a Single Control-Plane Cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)
